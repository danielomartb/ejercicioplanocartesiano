/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import ufps.util.varios.*;

// 1151859
// * @author Daniel Omar Tijaro Beltran
//* Estructuras de Datos - C
// * Ing Sistemas
//* 19 - 03 - 2021
//* 12:42 am
public class PruebaLeerURL {
    
    public static void main(String[] args) {
        ArchivoLeerURL archivo=new ArchivoLeerURL("https://gitlab.com/danielomartb/coordenascartesianas/-/raw/master/puntos.csv");//puse la URL desde mi GitLab 
        //ArchivoLeerURL archivo=new ArchivoLeerURL("http://www.madarme.co");
        Object datos[]=archivo.leerArchivo();
        for(Object filas:datos)
            System.out.println(filas.toString());
    }
    
}
