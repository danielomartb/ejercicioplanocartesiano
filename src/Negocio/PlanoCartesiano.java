/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import ufps.util.varios.ArchivoLeerURL;

//* 1151859
//* @author Daniel Omar Tijaro Beltran
//* Estructuras de Datos - C
//* Ing Sistemas
//* 19 - 03 - 2021
//* 12:42 am
public class PlanoCartesiano {
    
    // (5,7)...(-9,6)
    
    private String urlPuntos;
    private Funcion funciones[];

    public PlanoCartesiano() {
    }

    
    public PlanoCartesiano(String urlPuntos) {
        ArchivoLeerURL archivo=new ArchivoLeerURL("https://gitlab.com/danielomartb/coordenascartesianas/-/raw/master/puntos.csv");//puse la URL desde mi GitLab 
        Object datos[]=archivo.leerArchivo();
        //No se toma en cuenta la primera línea del archivo , ya que está contiene el formato de los datos
        //Crear los espacios para las funciones:
        this.funciones=new Funcion[datos.length-1];
    }

    
    
    
    public Funcion[] getFunciones() {
        return funciones;
    }

    public void setFunciones(Funcion[] funciones) {
        this.funciones = funciones;
    }
    
    
}
